package gitlab

import (
	"context"
	"fmt"
	"strconv"

	"github.com/go-kit/log"
	"github.com/go-kit/log/level"
	goGitLab "github.com/xanzy/go-gitlab"
)

func FetchProjectCount(ctx context.Context, c *goGitLab.Client, logger log.Logger) (int, error) {
	_, r, err := c.Projects.ListProjects(&goGitLab.ListProjectsOptions{}, goGitLab.WithContext(ctx))
	if err != nil {
		level.Error(logger).Log("event", "FetchProjectCount", "error", err)
		return 0, err
	}

	totalProjectCount := r.Response.Header.Get("x-total")

	level.Info(logger).Log("event", "FetchProjectCount", "msg", fmt.Sprintf("Total Number of Projects: %s", totalProjectCount))
	return strconv.Atoi(totalProjectCount)
}

func FetchProject(ctx context.Context, c *goGitLab.Client, logger log.Logger, projectID int) (*goGitLab.Project, error) {
	p, _, err := c.Projects.GetProject(projectID, &goGitLab.GetProjectOptions{}, goGitLab.WithContext(ctx))
	if err != nil {
		level.Error(logger).Log("event", "FetchProject", "error", err)
		return &goGitLab.Project{}, err
	}

	level.Info(logger).Log("event", "FetchProject", "project_id", p.ID)
	return p, nil
}
