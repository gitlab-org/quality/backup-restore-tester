package gitlab

import (
	"context"

	"github.com/go-kit/log"
	"github.com/go-kit/log/level"
	goGitLab "github.com/xanzy/go-gitlab"
)

func FetchRepository(ctx context.Context, c *goGitLab.Client, logger log.Logger, projectID int) ([]*goGitLab.TreeNode, error) {
	repo, _, err := c.Repositories.ListTree(projectID, &goGitLab.ListTreeOptions{}, goGitLab.WithContext(ctx))
	if err != nil {
		level.Error(logger).Log("event", "FetchRepository", "error", err)
		return []*goGitLab.TreeNode{}, err
	}

	level.Info(logger).Log("event", "FetchRepository", "project_id", projectID, "file_count", len(repo))
	return repo, nil
}
