package gitlab

import (
	"context"
	"fmt"
	"strconv"

	"github.com/go-kit/log"
	"github.com/go-kit/log/level"
	goGitLab "github.com/xanzy/go-gitlab"
)

func FetchUserCount(ctx context.Context, c *goGitLab.Client, logger log.Logger) (int, error) {
	_, r, err := c.Users.ListUsers(&goGitLab.ListUsersOptions{}, goGitLab.WithContext(ctx))
	if err != nil {
		level.Error(logger).Log("event", "FetchUserCount", "error", err)
		return 0, err
	}

	totalUserCount := r.Response.Header.Get("x-total")

	level.Info(logger).Log("event", "FetchUserCount", "msg", fmt.Sprintf("Total Number of Users: %s", totalUserCount))
	return strconv.Atoi(totalUserCount)
}
