package gitlab

import (
	"context"
	"fmt"
	"strconv"

	"github.com/go-kit/log"
	"github.com/go-kit/log/level"
	goGitLab "github.com/xanzy/go-gitlab"
)

func FetchSnippetCount(ctx context.Context, c *goGitLab.Client, logger log.Logger) (int, error) {
	_, r, err := c.Snippets.ListAllSnippets(&goGitLab.ListAllSnippetsOptions{}, goGitLab.WithContext(ctx))
	if err != nil {
		level.Error(logger).Log("event", "FetchSnippetCount", "error", err)
		return 0, err
	}

	totalSnippetCount := r.Response.Header.Get("x-total")

	level.Info(logger).Log("event", "FetchSnippetCount", "msg", fmt.Sprintf("Total Number of Snippets: %s", totalSnippetCount))
	return strconv.Atoi(totalSnippetCount)
}
