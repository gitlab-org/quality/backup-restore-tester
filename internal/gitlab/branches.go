package gitlab

import (
	"context"

	"github.com/go-kit/log"
	"github.com/go-kit/log/level"
	goGitLab "github.com/xanzy/go-gitlab"
)

func FetchBranches(ctx context.Context, c *goGitLab.Client, logger log.Logger, projectID int) ([]*goGitLab.Branch, error) {
	branches, _, err := c.Branches.ListBranches(projectID, &goGitLab.ListBranchesOptions{}, goGitLab.WithContext(ctx))
	if err != nil {
		level.Error(logger).Log("event", "FetchBranches", "error", err)
		return []*goGitLab.Branch{}, err
	}

	level.Info(logger).Log("event", "FetchBranches", "project_id", projectID, "branch_count", len(branches))
	return branches, nil
}
