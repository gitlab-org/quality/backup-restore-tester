package gitlab

import (
	"context"
	"fmt"
	"strconv"

	"github.com/go-kit/log"
	"github.com/go-kit/log/level"
	goGitLab "github.com/xanzy/go-gitlab"
)

func FetchMergeRequestCount(ctx context.Context, c *goGitLab.Client, logger log.Logger) (int, error) {
	_, r, err := c.MergeRequests.ListMergeRequests(&goGitLab.ListMergeRequestsOptions{}, goGitLab.WithContext(ctx))
	if err != nil {
		level.Error(logger).Log("event", "FetchMergeRequestCount", "error", err)
		return 0, err
	}

	totalMergeRequestCount := r.Response.Header.Get("x-total")

	level.Info(logger).Log("event", "FetchMergeRequestCount", "msg", fmt.Sprintf("Total Number of Merge Requests: %s", totalMergeRequestCount))
	return strconv.Atoi(totalMergeRequestCount)
}

func FetchMergeRequests(ctx context.Context, c *goGitLab.Client, logger log.Logger, projectID int) ([]*goGitLab.MergeRequest, error) {
	mrs, _, err := c.MergeRequests.ListProjectMergeRequests(projectID, &goGitLab.ListProjectMergeRequestsOptions{}, goGitLab.WithContext(ctx))
	if err != nil {
		level.Error(logger).Log("event", "FetchMergeRequests", "error", err)
		return []*goGitLab.MergeRequest{}, err
	}

	level.Info(logger).Log("event", "FetchMergeRequests", "project_id", projectID, "mr_count", len(mrs))
	return mrs, nil
}
