package gitlab

import (
	"context"

	"github.com/go-kit/log"
	"github.com/go-kit/log/level"
	goGitLab "github.com/xanzy/go-gitlab"
)

func FetchMilestones(ctx context.Context, c *goGitLab.Client, logger log.Logger, projectID int) ([]*goGitLab.Milestone, error) {
	milestones, _, err := c.Milestones.ListMilestones(projectID, &goGitLab.ListMilestonesOptions{}, goGitLab.WithContext(ctx))
	if err != nil {
		level.Error(logger).Log("event", "FetchMilestones", "error", err)
		return []*goGitLab.Milestone{}, err
	}

	level.Info(logger).Log("event", "FetchMilestones", "project_id", projectID, "milestone_count", len(milestones))
	return milestones, nil
}
