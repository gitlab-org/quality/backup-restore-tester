package gitlab

import (
	"context"

	"github.com/go-kit/log"
	"github.com/go-kit/log/level"
	goGitLab "github.com/xanzy/go-gitlab"
)

func FetchNotes(ctx context.Context, c *goGitLab.Client, logger log.Logger, projectID int, issueID int) ([]*goGitLab.Note, error) {
	notes, _, err := c.Notes.ListIssueNotes(projectID, issueID, &goGitLab.ListIssueNotesOptions{}, goGitLab.WithContext(ctx))
	if err != nil {
		level.Error(logger).Log("event", "FetchNotes", "error", err)
		return []*goGitLab.Note{}, err
	}

	level.Info(logger).Log("event", "FetchNotes", "project_id", projectID, "issue_id", issueID, "note_count", len(notes))
	return notes, nil
}
