package gitlab

import (
	"context"
	"fmt"
	"strconv"

	"github.com/go-kit/log"
	"github.com/go-kit/log/level"
	goGitLab "github.com/xanzy/go-gitlab"
)

func FetchGroupCount(ctx context.Context, c *goGitLab.Client, logger log.Logger) (int, error) {
	_, r, err := c.Groups.ListGroups(&goGitLab.ListGroupsOptions{}, goGitLab.WithContext(ctx))
	if err != nil {
		level.Error(logger).Log("event", "FetchGroupCount", "error", err)
		return 0, err
	}

	totalGroupCount := r.Response.Header.Get("x-total")

	level.Info(logger).Log("event", "FetchGroupCount", "msg", fmt.Sprintf("Total Number of Groups: %s", totalGroupCount))
	return strconv.Atoi(totalGroupCount)
}
