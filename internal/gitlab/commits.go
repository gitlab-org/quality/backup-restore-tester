package gitlab

import (
	"context"

	"github.com/go-kit/log"
	"github.com/go-kit/log/level"
	goGitLab "github.com/xanzy/go-gitlab"
)

func FetchCommits(ctx context.Context, c *goGitLab.Client, logger log.Logger, projectID int) ([]*goGitLab.Commit, error) {
	commits, _, err := c.Commits.ListCommits(projectID, &goGitLab.ListCommitsOptions{}, goGitLab.WithContext(ctx))
	if err != nil {
		level.Error(logger).Log("event", "FetchCommits", "error", err)
		return []*goGitLab.Commit{}, err
	}

	level.Info(logger).Log("event", "FetchCommits", "project_id", projectID, "commit_count", len(commits))
	return commits, nil
}
