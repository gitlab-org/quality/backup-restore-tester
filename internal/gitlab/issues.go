package gitlab

import (
	"context"
	"fmt"
	"strconv"

	"github.com/go-kit/log"
	"github.com/go-kit/log/level"
	goGitLab "github.com/xanzy/go-gitlab"
)

func FetchIssueCount(ctx context.Context, c *goGitLab.Client, logger log.Logger) (int, error) {
	opts := &goGitLab.ListIssuesOptions{
		Scope: goGitLab.Ptr("all"),
	}
	_, r, err := c.Issues.ListIssues(opts, goGitLab.WithContext(ctx))
	if err != nil {
		level.Error(logger).Log("event", "FetchIssueCount", "error", err)
		return 0, err
	}

	totalIssueCount := r.Response.Header.Get("x-total")

	level.Info(logger).Log("event", "FetchIssueCount", "msg", fmt.Sprintf("Total Number of Issues: %s", totalIssueCount))
	return strconv.Atoi(totalIssueCount)
}

func FetchIssues(ctx context.Context, c *goGitLab.Client, logger log.Logger, projectID int) ([]*goGitLab.Issue, error) {
	issues, _, err := c.Issues.ListProjectIssues(projectID, &goGitLab.ListProjectIssuesOptions{}, goGitLab.WithContext(ctx))
	if err != nil {
		level.Error(logger).Log("event", "ListProjectIssues", "error", err)
		return []*goGitLab.Issue{}, err
	}

	level.Info(logger).Log("event", "FetchIssues", "project_id", projectID, "issue_count", len(issues))
	return issues, nil
}
