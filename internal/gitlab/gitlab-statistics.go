package gitlab

type Statistics struct {
	GroupCount        int `json:"group_count"`
	IssueCount        int `json:"issue_count"`
	MergeRequestCount int `json:"merge_request_count"`
	ProjectCount      int `json:"project_count"`
	SnippetCount      int `json:"snippet_count"`
	UserCount         int `json:"user_count"`
}
