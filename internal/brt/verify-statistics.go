package brt

import (
	"context"
	"fmt"

	"github.com/go-kit/log"
	"github.com/go-kit/log/level"
	"github.com/go-test/deep"
	goGitLab "github.com/xanzy/go-gitlab"
	"gitlab.com/gitlab-org/quality/backup-restore-tester/internal/gitlab"
)

func VerifyStatistics(ctx context.Context, gitlabClient *goGitLab.Client, logger log.Logger) error {
	savedGitLabStats, err := loadFromJSONFile[*gitlab.Statistics](GitlabStatisticsFileName, logger)
	if err != nil {
		level.Error(logger).Log("event", "loadFromJSONFile", "error", err)
		return err
	}

	gitlabStats, err := fetchGitLabStats(ctx, gitlabClient, logger)
	if err != nil {
		level.Error(logger).Log("event", "fetchGitLabStats", "error", err)
		return err
	}

	diff := deep.Equal(gitlabStats, savedGitLabStats)
	if diff != nil {
		return fmt.Errorf("%v", diff)
	}

	level.Info(logger).Log("event", "verifyStatistics complete", "msg", "All values compared and are equal.")
	return nil
}
