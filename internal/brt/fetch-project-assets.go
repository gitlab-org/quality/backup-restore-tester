package brt

import (
	"context"
	"fmt"

	"github.com/go-kit/log"
	"github.com/go-kit/log/level"
	goGitLab "github.com/xanzy/go-gitlab"
	"gitlab.com/gitlab-org/quality/backup-restore-tester/internal/gitlab"
)

func FetchAssets(ctx context.Context, gitlabClient *goGitLab.Client, projectID int, random int, logger log.Logger) error {
	var (
		assets gitlabAssets
		err    error
	)

	switch {
	case projectID > 0:
		assets, err = fetchProject(ctx, gitlabClient, projectID, logger)
	case random > 0:
		assets, err = fetchRandomProjects(ctx, gitlabClient, random, logger)
	default:
		return fmt.Errorf("no project ID or number of random projects to fetch provided")
	}
	if err != nil {
		level.Error(logger).Log("event", "FetchAssets", "error", err)
		return err
	}

	err = saveGitLabAssets(assets, logger)
	if err != nil {
		level.Error(logger).Log("event", "saveGitLabAssets", "error", err)
		return err
	}

	return err
}

func fetchProject(ctx context.Context, gitlabClient *goGitLab.Client, projectID int, logger log.Logger) (gitlabAssets, error) {
	var assets gitlabAssets

	project, err := gitlab.FetchProject(ctx, gitlabClient, logger, projectID)
	if err != nil {
		level.Error(logger).Log("event", "fetchProject", "error", err)
		return assets, err
	}

	assets.Projects = []*goGitLab.Project{project}
	err = fetchProjectAssets(ctx, gitlabClient, &assets, logger)
	if err != nil {
		level.Error(logger).Log("event", "fetchProjectAssets", "error", err)
		return assets, err
	}

	return assets, nil
}

func fetchProjectAssets(ctx context.Context, gitlabClient *goGitLab.Client, assets *gitlabAssets, logger log.Logger) error {
	for _, project := range assets.Projects {
		branches, err := gitlab.FetchBranches(ctx, gitlabClient, logger, project.ID)
		if err != nil {
			level.Error(logger).Log("event", "FetchBranches", "error", err)
			return err
		}

		commits, err := gitlab.FetchCommits(ctx, gitlabClient, logger, project.ID)
		if err != nil {
			level.Error(logger).Log("event", "FetchCommits", "error", err)
			return err
		}

		issues, err := gitlab.FetchIssues(ctx, gitlabClient, logger, project.ID)
		if err != nil {
			level.Error(logger).Log("event", "FetchIssues", "error", err)
			return err
		}

		for _, issue := range issues {
			notes, err := gitlab.FetchNotes(ctx, gitlabClient, logger, issue.ProjectID, issue.IID)
			if err != nil {
				level.Error(logger).Log("event", "FetchNotes", "error", err)
				return err
			}

			assets.Notes = append(assets.Notes, notes...)
		}

		mergeRequests, err := gitlab.FetchMergeRequests(ctx, gitlabClient, logger, project.ID)
		if err != nil {
			level.Error(logger).Log("event", "FetchMergeRequests", "error", err)
			return err
		}

		milestones, err := gitlab.FetchMilestones(ctx, gitlabClient, logger, project.ID)
		if err != nil {
			level.Error(logger).Log("event", "FetchMilestones", "error", err)
			return err
		}

		repositories, err := gitlab.FetchRepository(ctx, gitlabClient, logger, project.ID)
		if err != nil {
			level.Error(logger).Log("event", "FetchRepository", "error", err)
			return err
		}

		assets.Branches = append(assets.Branches, branches...)
		assets.Commits = append(assets.Commits, commits...)
		assets.Issues = append(assets.Issues, issues...)
		assets.MergeRequests = append(assets.MergeRequests, mergeRequests...)
		assets.Milestones = append(assets.Milestones, milestones...)
		assets.Repositories = append(assets.Repositories, repositories...)
	}

	return nil
}

func fetchProjects(ctx context.Context, gitlabClient *goGitLab.Client, projectIDs []int, logger log.Logger) (gitlabAssets, error) {
	var assets gitlabAssets

	for i := 0; i < len(projectIDs); i++ {
		a, err := fetchProject(ctx, gitlabClient, projectIDs[i], logger)
		if err != nil {
			level.Error(logger).Log("event", "fetchProject", "error", err)
			return assets, err
		}
		assets = appendAssets(assets, a)
	}

	return assets, nil
}

func fetchRandomProjects(ctx context.Context, gitlabClient *goGitLab.Client, random int, logger log.Logger) (gitlabAssets, error) {
	var (
		assets gitlabAssets
		err    error
	)

	projectCount, err := gitlab.FetchProjectCount(ctx, gitlabClient, logger)
	if err != nil {
		level.Error(logger).Log("event", "FetchGroupCount", "error", err)
		return gitlabAssets{}, err
	}

	ids := generateUniqueRandomNumbers(random, projectCount)
	err = saveToJSONFile(ids, GitLabProjectIDsFileName, logger)
	if err != nil {
		level.Error(logger).Log("event", "saveProjectIDs", "error", err)
		return assets, err
	}

	assets, err = fetchProjects(ctx, gitlabClient, ids, logger)
	if err != nil {
		level.Error(logger).Log("event", "fetchProjects", "error", err)
		return assets, err
	}

	return assets, nil
}

func appendAssets(a gitlabAssets, b gitlabAssets) gitlabAssets {
	return gitlabAssets{
		Branches:      append(a.Branches, b.Branches...),
		Commits:       append(a.Commits, b.Commits...),
		Issues:        append(a.Issues, b.Issues...),
		MergeRequests: append(a.MergeRequests, b.MergeRequests...),
		Milestones:    append(a.Milestones, b.Milestones...),
		Notes:         append(a.Notes, b.Notes...),
		Projects:      append(a.Projects, b.Projects...),
		Repositories:  append(a.Repositories, b.Repositories...),
	}
}

func saveGitLabAssets(assets gitlabAssets, logger log.Logger) error {
	err := saveToJSONFile(assets.Branches, GitLabBranchesFileName, logger)
	if err != nil {
		level.Error(logger).Log("event", "saveBranches", "error", err)
		return err
	}

	err = saveToJSONFile(assets.Commits, GitLabCommitsFileName, logger)
	if err != nil {
		level.Error(logger).Log("event", "saveCommits", "error", err)
		return err
	}

	err = saveToJSONFile(assets.Issues, GitLabIssuesFileName, logger)
	if err != nil {
		level.Error(logger).Log("event", "saveIssues", "error", err)
		return err
	}

	err = saveToJSONFile(assets.MergeRequests, GitLabMergeRequestsFileName, logger)
	if err != nil {
		level.Error(logger).Log("event", "saveMergeRequests", "error", err)
		return err
	}

	err = saveToJSONFile(assets.Milestones, GitLabMilestonesFileName, logger)
	if err != nil {
		level.Error(logger).Log("event", "saveMilestones", "error", err)
		return err
	}

	err = saveToJSONFile(assets.Notes, GitLabNotesFileName, logger)
	if err != nil {
		level.Error(logger).Log("event", "saveNotes", "error", err)
		return err
	}

	err = saveToJSONFile(assets.Projects, GitLabProjectsFileName, logger)
	if err != nil {
		level.Error(logger).Log("event", "saveProjects", "error", err)
		return err
	}

	err = saveToJSONFile(assets.Repositories, GitLabRepositoriesFileName, logger)
	if err != nil {
		level.Error(logger).Log("event", "saveRepositories", "error", err)
		return err
	}

	return nil
}
