package brt

import (
	"context"
	"fmt"

	"github.com/go-kit/log"
	"github.com/go-kit/log/level"
	"github.com/go-test/deep"
	goGitLab "github.com/xanzy/go-gitlab"
)

func VerifyProjects(ctx context.Context, gitlabClient *goGitLab.Client, logger log.Logger) error {
	ids, err := loadFromJSONFile[[]int](GitLabProjectIDsFileName, logger)
	if err != nil {
		level.Error(logger).Log("event", "loadFromJSONFile", "error", err)
		return err
	}

	savedAssets, err := fetchProjects(ctx, gitlabClient, ids, logger)
	if err != nil {
		level.Error(logger).Log("event", "fetchProjects", "error", err)
		return err
	}

	assets, err := loadGitLabAssets(logger)
	if err != nil {
		level.Error(logger).Log("event", "loadFromJSONFile", "error", err)
		return err
	}

	diff := deep.Equal(assets, savedAssets)
	if diff != nil {
		return fmt.Errorf("%v", diff)
	}

	level.Info(logger).Log("event", "VerifyProjects complete", "msg", "All values compared and are equal.")
	return nil
}

func loadGitLabAssets(logger log.Logger) (gitlabAssets, error) {
	branches, err := loadFromJSONFile[[]*goGitLab.Branch](GitLabBranchesFileName, logger)
	if err != nil {
		level.Error(logger).Log("event", "loadBranches", "error", err)
		return gitlabAssets{}, err
	}

	commits, err := loadFromJSONFile[[]*goGitLab.Commit](GitLabCommitsFileName, logger)
	if err != nil {
		level.Error(logger).Log("event", "loadCommits", "error", err)
		return gitlabAssets{}, err
	}

	issues, err := loadFromJSONFile[[]*goGitLab.Issue](GitLabIssuesFileName, logger)
	if err != nil {
		level.Error(logger).Log("event", "loadIssues", "error", err)
		return gitlabAssets{}, err
	}

	mergeRequests, err := loadFromJSONFile[[]*goGitLab.MergeRequest](GitLabMergeRequestsFileName, logger)
	if err != nil {
		level.Error(logger).Log("event", "loadMergeRequests", "error", err)
		return gitlabAssets{}, err
	}

	milestones, err := loadFromJSONFile[[]*goGitLab.Milestone](GitLabMilestonesFileName, logger)
	if err != nil {
		level.Error(logger).Log("event", "loadMilestones", "error", err)
		return gitlabAssets{}, err
	}

	notes, err := loadFromJSONFile[[]*goGitLab.Note](GitLabNotesFileName, logger)
	if err != nil {
		level.Error(logger).Log("event", "loadNotes", "error", err)
		return gitlabAssets{}, err
	}

	projects, err := loadFromJSONFile[[]*goGitLab.Project](GitLabProjectsFileName, logger)
	if err != nil {
		level.Error(logger).Log("event", "loadProjects", "error", err)
		return gitlabAssets{}, err
	}

	repositories, err := loadFromJSONFile[[]*goGitLab.TreeNode](GitLabRepositoriesFileName, logger)
	if err != nil {
		level.Error(logger).Log("event", "loadRepositories", "error", err)
		return gitlabAssets{}, err
	}

	return gitlabAssets{
		Branches:      branches,
		Commits:       commits,
		Issues:        issues,
		MergeRequests: mergeRequests,
		Milestones:    milestones,
		Notes:         notes,
		Projects:      projects,
		Repositories:  repositories,
	}, nil
}
