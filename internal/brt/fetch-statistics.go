package brt

import (
	"context"
	"fmt"

	"github.com/go-kit/log"
	"github.com/go-kit/log/level"
	goGitLab "github.com/xanzy/go-gitlab"
	"gitlab.com/gitlab-org/quality/backup-restore-tester/internal/gitlab"
)

func FetchStatistics(ctx context.Context, gitlabClient *goGitLab.Client, logger log.Logger) error {
	gitlabStats, err := fetchGitLabStats(ctx, gitlabClient, logger)
	if err != nil {
		level.Error(logger).Log("event", "fetchGitLabStats", "error", err)
		return err
	}

	err = saveToJSONFile(gitlabStats, GitlabStatisticsFileName, logger)
	if err != nil {
		level.Error(logger).Log("event", "saveGitLabStats", "error", err)
		return err
	}

	level.Info(logger).Log("event", "fetchStatistics complete", "msg", fmt.Sprintf("All values fetched and stored in %v", GitlabStatisticsFileName))
	return nil
}

func fetchGitLabStats(ctx context.Context, gitlabClient *goGitLab.Client, logger log.Logger) (*gitlab.Statistics, error) {
	groupCount, err := gitlab.FetchGroupCount(ctx, gitlabClient, logger)
	if err != nil {
		level.Error(logger).Log("event", "FetchGroupCount", "error", err)
		return &gitlab.Statistics{}, err
	}

	issueCount, err := gitlab.FetchIssueCount(ctx, gitlabClient, logger)
	if err != nil {
		level.Error(logger).Log("event", "FetchIssueCount", "error", err)
		return &gitlab.Statistics{}, err
	}

	mergeRequestCount, err := gitlab.FetchMergeRequestCount(ctx, gitlabClient, logger)
	if err != nil {
		level.Error(logger).Log("event", "FetchMergeRequestCount", "error", err)
		return &gitlab.Statistics{}, err
	}

	projectCount, err := gitlab.FetchProjectCount(ctx, gitlabClient, logger)
	if err != nil {
		level.Error(logger).Log("event", "FetchProjectCount", "error", err)
		return &gitlab.Statistics{}, err
	}

	snippetCount, err := gitlab.FetchSnippetCount(ctx, gitlabClient, logger)
	if err != nil {
		level.Error(logger).Log("event", "FetchSnippetCount", "error", err)
		return &gitlab.Statistics{}, err
	}

	userCount, err := gitlab.FetchUserCount(ctx, gitlabClient, logger)
	if err != nil {
		level.Error(logger).Log("event", "userCount", "error", err)
		return &gitlab.Statistics{}, err
	}

	return &gitlab.Statistics{
		GroupCount:        groupCount,
		IssueCount:        issueCount,
		MergeRequestCount: mergeRequestCount,
		ProjectCount:      projectCount,
		SnippetCount:      snippetCount,
		UserCount:         userCount,
	}, nil
}
