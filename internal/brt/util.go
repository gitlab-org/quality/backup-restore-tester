package brt

import (
	"encoding/json"
	"io"
	"math/rand"
	"os"

	"github.com/go-kit/log"
	"github.com/go-kit/log/level"
	goGitLab "github.com/xanzy/go-gitlab"
)

const (
	GitLabBranchesFileName      = "gitlab-branches.json"
	GitLabCommitsFileName       = "gitlab-commits.json"
	GitLabIssuesFileName        = "gitlab-issues.json"
	GitLabMergeRequestsFileName = "gitlab-merge-requests.json"
	GitLabMilestonesFileName    = "gitlab-milestones.json"
	GitLabNotesFileName         = "gitlab-notes.json"
	GitLabProjectIDsFileName    = "gitlab-project-ids.json"
	GitLabProjectsFileName      = "gitlab-projects.json"
	GitLabRepositoriesFileName  = "gitlab-repositories.json"
	GitlabStatisticsFileName    = "gitlab-statistics.json"
)

type gitlabAssets struct {
	Branches      []*goGitLab.Branch       `json:"branches"`
	Commits       []*goGitLab.Commit       `json:"commits"`
	Issues        []*goGitLab.Issue        `json:"issues"`
	MergeRequests []*goGitLab.MergeRequest `json:"merge_requests"`
	Milestones    []*goGitLab.Milestone    `json:"milestones"`
	Notes         []*goGitLab.Note         `json:"notes"`
	Projects      []*goGitLab.Project      `json:"projects"`
	Repositories  []*goGitLab.TreeNode     `json:"repositories"`
}

func saveToJSONFile(data interface{}, fileName string, logger log.Logger) error {
	jsonData, err := json.MarshalIndent(data, "", "    ")
	if err != nil {
		level.Error(logger).Log("event", "json.MarshalIndent", "error", err)
		return err
	}

	err = os.WriteFile(fileName, jsonData, 0644)
	if err != nil {
		level.Error(logger).Log("event", "os.WriteFile", "error", err)
		return err
	}

	return nil
}

func loadFromJSONFile[T any](fileName string, logger log.Logger) (T, error) {
	var t T

	jsonData, err := os.Open(fileName)
	if err != nil {
		level.Error(logger).Log("event", "os.Open", "error", err)
		return t, err
	}
	defer jsonData.Close()

	b, err := io.ReadAll(jsonData)
	if err != nil {
		level.Error(logger).Log("event", "io.ReadAll", "error", err)
		return t, err
	}

	return t, json.Unmarshal(b, &t)
}

func generateUniqueRandomNumbers(n, max int) []int {
	set := make(map[int]bool)
	var result []int
	for len(set) < n {
		value := rand.Intn(max) + 1
		if !set[value] {
			set[value] = true
			result = append(result, value)
		}
	}
	return result
}
