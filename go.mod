module gitlab.com/gitlab-org/quality/backup-restore-tester

go 1.22.4

require (
	github.com/alecthomas/kong v0.9.0
	github.com/go-kit/log v0.2.1
	github.com/go-test/deep v1.1.0
	github.com/xanzy/go-gitlab v0.105.0
)

require (
	github.com/go-logfmt/logfmt v0.5.1 // indirect
	github.com/golang/protobuf v1.5.3 // indirect
	github.com/google/go-querystring v1.1.0 // indirect
	github.com/hashicorp/go-cleanhttp v0.5.2 // indirect
	github.com/hashicorp/go-retryablehttp v0.7.7 // indirect
	github.com/stretchr/testify v1.8.2 // indirect
	golang.org/x/net v0.8.0 // indirect
	golang.org/x/oauth2 v0.6.0 // indirect
	golang.org/x/time v0.3.0 // indirect
	google.golang.org/appengine v1.6.7 // indirect
	google.golang.org/protobuf v1.29.1 // indirect
)
