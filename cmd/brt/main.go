package main

import (
	"context"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"

	"github.com/alecthomas/kong"
	"github.com/go-kit/log"
	"github.com/go-kit/log/level"
	goGitLab "github.com/xanzy/go-gitlab"
	"gitlab.com/gitlab-org/quality/backup-restore-tester/internal/brt"
)

var CLI struct {
	APIToken  string `env:"API_TOKEN"  help:"API Token for authentication for the GitLab instance." name:"api-token"  short:"t"`
	GitLabURL string `env:"GITLAB_URL" help:"URL for the GitLab instance."                          name:"gitlab-url" short:"u"`

	Fetch struct {
		Projects struct {
			ID     int `default:"0" help:"Fetch single project."    name:"id"     short:"i"`
			Random int `default:"0" help:"Fetch x random projects." name:"random" short:"r"`
		} `cmd:"projects" help:"Fetch all assets related to a project and save for verification later. Specify a specific project ID or a number of random projects to fetch."`
		Stats struct{} `cmd:"stats"    help:"Fetch GitLab Statistics as shown on the admin overview page and save for verification later."`
	} `cmd:"fetch" help:"Fetch information from the GitLab instance and save for verification later."`

	Verify struct {
		Projects struct{} `cmd:"projects" help:"Verify saved project IDs match."`
		Stats    struct{} `cmd:"stats"    help:"Verify GitLab Statistics as shown on the admin overview page."`
	} `cmd:"verify" help:"Load previously saved data and compare with the GitLab instance."`
}

func main() {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)

	go func() {
		<-sigs
		cancel()
	}()

	logger := log.NewLogfmtLogger(log.NewSyncWriter(os.Stdout))
	logger = log.With(logger, "ts", log.DefaultTimestampUTC)
	logger = level.NewInjector(logger, level.InfoValue())

	opts := kong.Parse(&CLI)
	gitlabClient, err := goGitLab.NewClient(
		CLI.APIToken,
		goGitLab.WithBaseURL(CLI.GitLabURL),
		goGitLab.WithCustomRetryMax(18),
		goGitLab.WithCustomRetryWaitMinMax(10*time.Second, 10*time.Second),
	)
	if err != nil {
		level.Error(logger).Log("event", "goGitLab.NewClient", "error", err)
		fatal(logger, err)
	}

	switch opts.Command() {
	case "fetch projects":
		fatal(logger, brt.FetchAssets(ctx, gitlabClient, CLI.Fetch.Projects.ID, CLI.Fetch.Projects.Random, logger))
	case "fetch stats":
		fatal(logger, brt.FetchStatistics(ctx, gitlabClient, logger))
	case "verify projects":
		fatal(logger, brt.VerifyProjects(ctx, gitlabClient, logger))
	case "verify stats":
		fatal(logger, brt.VerifyStatistics(ctx, gitlabClient, logger))
	default:
		panic(opts.Command())
	}
}

func fatal(logger log.Logger, err error) {
	if err != nil {
		level.Error(logger).Log("event", "shutdown", "error", err)
		if strings.Contains(err.Error(), "context canceled") {
			os.Exit(0)
		}
		os.Exit(1)
	}
}
