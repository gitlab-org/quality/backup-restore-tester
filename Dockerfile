FROM golang:1.22.4-alpine

WORKDIR $GOPATH/src/gitlab.com/gitlab-org/quality/backup-restore-tester

COPY . .
RUN go mod download
RUN go build ./cmd/brt

CMD [ "sh" ]